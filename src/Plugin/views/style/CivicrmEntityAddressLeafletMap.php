<?php

namespace Drupal\civicrm_entity_leaflet\Plugin\views\style;

use Drupal\search_api\Plugin\views\ResultRow;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Render\RenderContext;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Entity\Plugin\DataType\EntityAdapter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\leaflet_views\Controller\LeafletAjaxPopupController;
use Drupal\Core\Url;
use Drupal\Component\Utility\Html;
use Drupal\leaflet_views\Plugin\views\style\LeafletMap;
use Drupal\views\Plugin\views\PluginBase;
use Drupal\views\Plugin\views\style\StylePluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Style plugin to render a View output as a Leaflet map.
 *
 * @ingroup views_style_plugins
 *
 * Attributes set below end up in the $this->definition[] array.
 *
 * @ViewsStyle(
 *   id = "civicrm_entity_address_leaflet_map",
 *   title = @Translation("CiviCRM Entity Address Leaflet Map"),
 *   help = @Translation("Displays a View as a Leaflet map."),
 *   display_types = {"normal"},
 *   theme = "leaflet-map"
 * )
 */
class CivicrmEntityAddressLeafletMap extends LeafletMap implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('entity_display.repository'),
      $container->get('current_user'),
      $container->get('messenger'),
      $container->get('renderer'),
      $container->get('module_handler'),
      $container->get('leaflet.service'),
      $container->get('link_generator'),
      $container->get('plugin.manager.field.field_type')
    );
  }

  /**
   * Get a list of fields and a sublist of geo data fields in this view.
   *
   * @return array
   *   Available data sources.
   */
  protected function getAvailableDataSources() {
    $fields_geo_data = [];

    /** @var \Drupal\views\Plugin\views\ViewsHandlerInterface $handler) */
    foreach ($this->displayHandler->getHandlers('field') as $field_id => $handler) {
      $label = $handler->adminLabel() ?: $field_id;
      $this->viewFields[$field_id] = $label;
      if (is_a($handler, '\Drupal\views\Plugin\views\field\EntityField')) {
        $fields_geo_data[$field_id] = $label;
      }
    }

    return $fields_geo_data;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {

    // If data source changed then apply the changes.
    if ($form_state->get('entity_source')) {
      $this->options['entity_source'] = $form_state->get('entity_source');
      $this->entityInfo = $this->getEntitySourceEntityInfo($this->options['entity_source']);
      $this->entityType = $this->entityInfo->id();
      $this->entitySource = $this->options['entity_source'];
    }

    StylePluginBase::buildOptionsForm($form, $form_state);

    $form['#attached'] = [
      'library' => [
        'leaflet/general',
      ],
    ];

    // Customise for this Leaflet View Style the "grouping" section.
    $form["grouping"] = [
      '#type' => 'details',
      '#title' => $this->t("Leaflet Grouping"),
      0 => $form["grouping"][0],
    ];

    $form["grouping"][0]["field"]["#title"] = $this->t('Grouping field');
    $form["grouping"][0]["field"]["#description"] = $this->t("You may optionally specify a field by which to group the Leaflet Map Features by Overlayers, whose visibility could be managed throughout the Leaflet Map Layers Control.<br>Leave blank to not group");
    unset($form["grouping"][0]["rendered_strip"]);

    $form["grouping"][0]["field"]['#ajax'] = [
      'callback' => __CLASS__ . '::updateGrouping0OverlaysOptionsAjax',
      'wrapper' => 'grouping-0-overlays_options-fieldset',
      'event' => 'change',
    ];

    $form["grouping"][0]["rendered"]['#ajax'] = [
      'callback' => __CLASS__ . '::updateGrouping0OverlaysOptionsAjax',
      'wrapper' => 'grouping-0-overlays_options-fieldset',
      'event' => 'change',
    ];

    // Unset/remove the Grouping Field n.2.
    // as we don't support it in Leaflet View style map, at the moment.
    unset($form["grouping"][1]);

    // Get a sublist of geo data fields in the view.
    $fields_geo_data = $this->getAvailableDataSources();

    // Overlay Options settings section.
    $form["grouping"][0]['overlays_options'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Layers options'),
      '#attributes' => ['id' => 'grouping-0-overlays_options-fieldset'],
    ];

    // Extract the Layers options depending on the form state.
    $grouping_0_field = $form_state->getUserInput()['style_options']['grouping'][0]['field'] ?? $form["grouping"][0]["field"]["#default_value"];
    $grouping_0_rendered_option = isset($form_state->getUserInput()['style_options']['grouping'][0]) ? ($form_state->getUserInput()['style_options']['grouping'][0]['rendered'] ?? FALSE) : $form["grouping"][0]["rendered"]["#default_value"];
    $overlays_options = self::getOverlaysOptions($this, $grouping_0_field, $grouping_0_rendered_option);

    // Disabled Layers.
    $form["grouping"][0]['overlays_options']['disabled_overlays'] = count($overlays_options) > 1 ? [
      '#type' => 'select',
      '#title' => $this->t('Disabled Layers'),
      '#description' => $this->t('Choose the Layers that should start as disabled / switched off'),
      '#options' => $overlays_options,
      '#default_value' => $this->options["grouping"][0]['overlays_options']['disabled_overlays'],
      // The #validated setting to TRUE skips the "An illegal choice has been
      // detected" error message after Ajax refresh.
      '#validated' => TRUE,
      '#required' => FALSE,
      '#multiple' => TRUE,
      '#size' => count($overlays_options) < 10 ? count($overlays_options) + 1 : 10,
      '#states' => [
        'invisible' => [
          ':input[name="style_options[grouping][0][field]"]' => ['value' => ''],
        ],
      ],
    ] : [
      '#type' => 'hidden',
      '#value' => [],
    ];

    // Disabled Layers.
    $form["grouping"][0]['overlays_options']['hidden_overlays_controls'] = count($overlays_options) > 1 ? [
      '#type' => 'select',
      '#title' => $this->t('Hidden Layers Controls'),
      '#description' => $this->t('Choose the Layers that will not appear in the Layers Control'),
      '#options' => $overlays_options,
      '#default_value' => $this->options["grouping"][0]['overlays_options']['hidden_overlays_controls'],
      // The #validated setting to TRUE skips the "An illegal choice has been
      // detected" error message after Ajax refresh.
      '#validated' => TRUE,
      '#required' => FALSE,
      '#multiple' => TRUE,
      '#size' => count($overlays_options) < 10 ? count($overlays_options) + 1 : 10,
      '#states' => [
        'invisible' => [
          ':input[name="style_options[grouping][0][field]"]' => ['value' => ''],
        ],
      ],
    ] : [
      '#type' => 'hidden',
      '#value' => [],
    ];

    // Check whether we have a geo data field we can work with.
    if (!count($fields_geo_data)) {
      $form['error'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => $this->t('Please add at least one Geofield to the View and come back here to set it as Data Source.'),
        '#attributes' => [
          'class' => ['leaflet-warning'],
        ],
      ];
      return;
    }

    $wrapper_id = 'leaflet-map-views-style-options-form-wrapper';
    $form['#prefix'] = '<div id="' . $wrapper_id . '">';
    $form['#suffix'] = '</div>';

    // Map preset.
    $form['data_source_lat'] = [
      '#type' => 'select',
      '#title' => $this->t('Data Source Latitude'),
      '#description' => $this->t('Which field contains the latitude?'),
      '#options' => $fields_geo_data,
      '#default_value' => $this->options['data_source_lat'],
      '#required' => TRUE,
    ];

    $form['data_source_lon'] = [
      '#type' => 'select',
      '#title' => $this->t('Data Source Longitude'),
      '#description' => $this->t('Which field contains longitude?'),
      '#options' => $fields_geo_data,
      '#default_value' => $this->options['data_source_lon'],
      '#required' => TRUE,
    ];

    // Get the possible entity sources.
    $entity_sources = $this->getAvailableEntitySources();

    // If there is only one entity source it will be the base entity, so don't
    // show the element to avoid confusing people.
    if (count($entity_sources) == 1) {
      $form['entity_source'] = [
        '#type' => 'value',
        '#value' => key($entity_sources),
      ];
    }
    else {
      $form['entity_source'] = [
        '#type' => 'select',
        '#title' => new TranslatableMarkup('Entity Source'),
        '#description' => new TranslatableMarkup('Select which Entity should be used as Leaflet Mapping base Entity.<br><u>Leave as "View Base Entity" to rely on default Views behaviour, and don\'t specifically needed otherwise</u>.'),
        '#options' => $entity_sources,
        '#default_value' => !empty($this->options['entity_source']) ? $this->options['entity_source'] : '__base_table',
        '#ajax' => [
          'wrapper' => $wrapper_id,
          'callback' => [static::class, 'optionsFormEntitySourceSubmitAjax'],
          'trigger_as' => ['name' => 'entity_source_submit'],
        ],
      ];
      $form['entity_source_submit'] = [
        '#type' => 'submit',
        '#value' => new TranslatableMarkup('Update Entity Source'),
        '#name' => 'entity_source_submit',
        '#submit' => [
          [static::class, 'optionsFormEntitySourceSubmit'],
        ],
        '#validate' => [],
        '#limit_validation_errors' => [
          ['style_options', 'entity_source'],
        ],
        '#attributes' => [
          'class' => ['js-hide'],
        ],
        '#ajax' => [
          'wrapper' => $wrapper_id,
          'callback' => [static::class, 'optionsFormEntitySourceSubmitAjax'],
        ],
      ];
    }

    // Set Leaflet Tooltip Element.
    $this->setTooltipElement($form, $this->options, $this->viewFields);

    // Set Simple Tooltip.
    $form['name_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Simple Tooltip'),
      '#description' => $this->t('Choose the field which will appear as as Simple Tooltip on mouse over each Leaflet feature.'),
      '#options' => array_merge(['' => ' - None - '], $this->viewFields),
      '#default_value' => $this->options['name_field'],
      '#states' => [
        'visible' => [
          'select[name="style_options[leaflet_tooltip][value]' => ['value' => ''],
        ],
      ],
    ];

    // Get the human-readable labels for the entity view modes.
    $view_mode_options = [];
    foreach ($this->entityDisplayRepository->getViewModes($this->entityType) as $key => $view_mode) {
      $view_mode_options[$key] = $view_mode['label'];
    }

    // Set Leaflet Popup Element.
    $this->setPopupElement($form, $this->options, $this->viewFields, $this->entityType, $view_mode_options);

    // Generate the Leaflet Map General Settings.
    $this->generateMapGeneralSettings($form, $this->options);

    // Generate the Leaflet Map Reset Control.
    $this->setResetMapViewControl($form, $this->options);

    // Generate the Leaflet Map Position Form Element.
    $form['map_position'] = $this->generateMapPositionElement($this->options['map_position']);

    // Generate the Leaflet Map weight/zIndex Form Element.
    $form['weight'] = $this->generateWeightElement($this->options['weight']);

    // Generate Icon form element.
    $icon_options = $this->options['icon'];
    $form['icon'] = $this->generateIconFormElement($icon_options);

    // Set Map Marker Cluster Element.
    $this->setMapMarkerclusterElement($form, $this->options, $this->viewFields);

    // Set Fullscreen Element.
    $this->setFullscreenElement($form, $this->options);

    // Set Map Geometries Options Element.
    $this->setMapPathOptionsElement($form, $this->options);

    // Set the Feature Additional Properties Element.
    $this->setFeatureAdditionalPropertiesElement($form, $this->options);

    // Set Locate User Position Control Element.
    $this->setLocateControl($form, $this->options);

    // Set Map Geocoder Control Element, if the Geocoder Module exists,
    // otherwise output a tip on Geocoder Module Integration.
    $this->setGeocoderMapControl($form, $this->options);

    // Set Map Lazy Load Element.
    $this->setMapLazyLoad($form, $this->options);

    unset($form["#pre_render"]);

  }

  /**
   * Renders the View.
   */
  public function render() {
    $features_groups = [];
    $element = [];

    // Collect bubbleable metadata when doing early rendering.
    $build_for_bubbleable_metadata = [];

    // Always render the map, otherwise ...
    $leaflet_map_style = !isset($this->options['leaflet_map']) ? $this->options['map'] : $this->options['leaflet_map'];
    $map = leaflet_map_get_info($leaflet_map_style);

    // Set Map additional map Settings.
    $this->setAdditionalMapOptions($map, $this->options);

    // Add a specific map id.
    $map['id'] = Html::getUniqueId("leaflet_map_view_" . $this->view->id() . '_' . $this->view->current_display);

    if (($lat_field_name = $this->options['data_source_lat']) && ($lon_field_name = $this->options['data_source_lon'])) {
      $this->renderFields($this->view->result);

      // Group the rows according to the grouping instructions, if specified.
      $view_results_groups = $this->renderGrouping(
        $this->view->result,
        $this->options['grouping'],
        TRUE
      );

      foreach ($view_results_groups as $group_label => $view_results_group) {
        $features_group = [];
        // Sanitize the Group Label from Tags and invisible characters.
        $group_label = str_replace(["\n", "\r"], "", strip_tags($group_label));

        foreach ($view_results_group['rows'] as $id => $result) {
          $lat_value = (array) $this->getFieldValue($result->index, $lat_field_name);
          $long_value = (array) $this->getFieldValue($result->index, $lon_field_name);

          // Allow other modules to add/alter the $geofield_value
          // and the $map.
          $leaflet_view_geofield_value_alter_context = [
            'leaflet_map_style' => $leaflet_map_style,
            'result' => $result,
            'leaflet_view_style' => $this,
          ];
          $this->moduleHandler->alter('leaflet_map_view_geofield_value', $geofield_value, $map, $leaflet_view_geofield_value_alter_context);

          if (!empty($lat_value) && !empty($long_value)) {
            $features = [
              [
                'type' => 'point',
                'lat' => $lat_value[0],
                'lon' => $long_value[0],
              ],
            ];

            $entity_id = NULL;
            $entity_type = NULL;
            $entity_language = NULL;
            if (!empty($result->_entity)) {
              // Entity API provides a plain entity object.
              $entity = $result->_entity;
              $entity_id = $entity->id();
              $entity_type = $entity->getEntityTypeId();
              $entity_language = $entity->language()->getId();
            }
            elseif (isset($result->_object)) {
              // Search API provides a TypedData EntityAdapter.
              $entity_adapter = $result->_object;
              if ($entity_adapter instanceof EntityAdapter) {
                $entity = $entity_adapter->getValue();
                $entity_id = $entity->id();
                $entity_type = $entity->getEntityTypeId();
                $entity_language = $entity->language()->getId();
              }
            }
            elseif ($result instanceof ResultRow) {
              $id = $result->_item->getId();
              $search_api_id_parts = explode(':', $result->_item->getId());
              $id_parts = explode('/', $search_api_id_parts[1]);
              $entity_id = $id_parts[1] ?? NULL;
              $entity_type = $id_parts[0] ?? NULL;
              $entity_language = $search_api_id_parts[2] ?? NULL;
            }

            // Render the entity with the selected view mode.
            if (!empty($entity_id) && !empty($entity_type)) {
              $entity_type_langcode_attribute = $entity_type . '_field_data_langcode';

              $view = $this->view;

              // Set the langcode to be used for rendering the entity.
              $rendering_language = $view->display_handler->getOption('rendering_language');
              $dynamic_renderers = [
                '***LANGUAGE_entity_translation***' => 'TranslationLanguageRenderer',
                '***LANGUAGE_entity_default***' => 'DefaultLanguageRenderer',
              ];
              if (isset($dynamic_renderers[$rendering_language])) {
                /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
                $langcode = $result->$entity_type_langcode_attribute ?? $entity_language;
              }
              else {
                if (strpos($rendering_language, '***LANGUAGE_') !== FALSE) {
                  $langcode = PluginBase::queryLanguageSubstitutions()[$rendering_language];
                }
                else {
                  // Specific langcode set.
                  $langcode = $rendering_language;
                }
              }

              // Define the Popup source and Popup view mode with backward
              // compatibility with Leaflet release < 2.x.
              $popup_source = !empty($this->options['description_field']) ? $this->options['description_field'] : ($this->options['leaflet_popup']['value'] ?? '');
              $popup_view_mode = !empty($this->options['view_mode']) ? $this->options['view_mode'] : $this->options['leaflet_popup']['view_mode'];

              switch ($popup_source) {
                case '#rendered_entity':
                  $build = $this->entityManager->getViewBuilder($entity_type)
                    ->view($entity, $popup_view_mode, $langcode);
                  $render_context = new RenderContext();
                  $popup_content = $this->renderer->executeInRenderContext($render_context, function () use (&$build) {
                    return $this->renderer->render($build, TRUE);
                  });
                  if (!$render_context->isEmpty()) {
                    $render_context->update($build_for_bubbleable_metadata);
                  }
                  break;

                case '#rendered_entity_ajax':
                  $parameters = [
                    'entity_type' => $entity_type,
                    'entity' => $entity_id,
                    'view_mode' => $popup_view_mode,
                    'langcode' => $langcode,
                  ];
                  $url = Url::fromRoute('leaflet_views.ajax_popup', $parameters);
                  $popup_content = sprintf('<div class="leaflet-ajax-popup" data-leaflet-ajax-popup="%s" %s></div>',
                    $url->toString(), LeafletAjaxPopupController::getPopupIdentifierAttribute($entity_type, $entity_id, $this->options['leaflet_popup']['view_mode'], $langcode));
                  $map['settings']['ajaxPoup'] = TRUE;
                  break;

                case '#rendered_view_fields':
                  // Normal rendering via view/row fields
                  // (with labels options, formatters, classes, etc.).
                  $render_row = [
                    "markup" => $this->view->rowPlugin->render($result),
                  ];
                  $popup_content = $this->renderer->renderPlain($render_row);
                  break;

                default:
                  // Row rendering of single specified field value (without
                  // labels).
                  $popup_content = !empty($popup_source) ? $this->rendered_fields[$result->index][$popup_source] : '';
              }

              // Eventually merge map icon definition
              // from hook_leaflet_map_info.
              if (!empty($map['icon'])) {
                $this->options['icon'] = $this->options['icon'] ?: [];

                // Remove empty icon options so that they might be replaced
                // by the ones set by the hook_leaflet_map_info.
                foreach ($this->options['icon'] as $k => $icon_option) {
                  if (empty($icon_option) || (is_array($icon_option) && $this->leafletService->multipleEmpty($icon_option))) {
                    unset($this->options['icon'][$k]);
                  }
                }
                $this->options['icon'] = array_replace($map['icon'], $this->options['icon']);
              }

              // Define possible tokens.
              $tokens = [];
              foreach ($this->rendered_fields[$result->index] as $field_name => $field_value) {
                $tokens[$field_name] = $field_value;
              }

              $icon_type = $this->options['icon']['iconType'] ?? 'marker';

              // Relates each result feature with additional properties.
              foreach ($features as &$feature) {

                // Attach pop-ups if we have a description field.
                // Add its entity id, so it might be referenced from
                // outside.
                $feature['entity_id'] = $entity_id;

                // Generate the weight feature property
                // (falls back to natural result ordering).
                $feature['weight'] = !empty($this->options['weight']) ? intval(str_replace([
                  "\n",
                  "\r",
                ], "", $this->viewsTokenReplace($this->options['weight'], $tokens))) : $id;

                // Attach pop-ups if we have a description field.
                if (!empty($popup_content)) {
                  $feature['popup']['value'] = $popup_content;
                  $feature['popup']['options'] = $this->options['leaflet_popup'] ? $this->options['leaflet_popup']['options'] : NULL;
                }

                // Attach tooltip data (value & options),
                // if tooltip value is not empty.
                if (!empty($this->options['leaflet_tooltip']['value'])) {
                  $feature['tooltip'] = $this->options['leaflet_tooltip'];
                  // Decode any entities because JS will encode them again,
                  // and we don't want double encoding.
                  $feature['tooltip']['value'] = !empty($this->options['leaflet_tooltip']['value']) ? Html::decodeEntities(($this->rendered_fields[$result->index][$this->options['leaflet_tooltip']['value']])) : '';
                }
                // Otherwise eventually attach simple title tooltip.
                elseif ($this->options['name_field']) {
                  // Decode any entities because JS will encode them again,
                  // and we don't want double encoding.
                  $feature['title'] = !empty($this->options['name_field']) ? Html::decodeEntities(($this->rendered_fields[$result->index][$this->options['name_field']])) : '';
                }

                // Eventually set the custom Marker icon (DivIcon, Icon Url
                // or Circle Marker).
                if ($feature['type'] === 'point' && isset($this->options['icon'])) {
                  // Set Feature Icon properties.
                  $feature['icon'] = $this->options['icon'];

                  // Transforms Icon Options that support Replacement
                  // Patterns/Tokens.
                  if (!empty($this->options["icon"]["iconSize"]["x"])) {
                    $feature['icon']["iconSize"]["x"] = $this->viewsTokenReplace($this->options["icon"]["iconSize"]["x"], $tokens);
                  }
                  if (!empty($this->options["icon"]["iconSize"]["y"])) {
                    $feature['icon']["iconSize"]["y"] = $this->viewsTokenReplace($this->options["icon"]["iconSize"]["y"], $tokens);
                  }
                  if (!empty($this->options["icon"]["shadowSize"]["x"])) {
                    $feature['icon']["shadowSize"]["x"] = $this->viewsTokenReplace($this->options["icon"]["shadowSize"]["x"], $tokens);
                  }
                  if (!empty($this->options["icon"]["shadowSize"]["y"])) {
                    $feature['icon']["shadowSize"]["y"] = $this->viewsTokenReplace($this->options["icon"]["shadowSize"]["y"], $tokens);
                  }

                  switch ($icon_type) {
                    case 'html':
                      $feature['icon']['html'] = str_replace([
                        "\n",
                        "\r",
                      ], "", $this->viewsTokenReplace($this->options['icon']['html'], $tokens));
                      $feature['icon']['html_class'] = $this->options['icon']['html_class'];
                      break;

                    case 'circle_marker':
                      $feature['icon']['options'] = str_replace([
                        "\n",
                        "\r",
                      ], "", $this->viewsTokenReplace($this->options['icon']['circle_marker_options'], $tokens));
                      break;

                    default:
                      // Apply Token Replacements to iconUrl & shadowUrl.
                      if (!empty($this->options['icon']['iconUrl'])) {
                        $feature['icon']['iconUrl'] = str_replace([
                          "\n",
                          "\r",
                        ], "", $this->viewsTokenReplace($this->options['icon']['iconUrl'], $tokens));
                        // Generate correct Absolute iconUrl & shadowUrl,
                        // if not external.
                        if (!empty($feature['icon']['iconUrl'])) {
                          $feature['icon']['iconUrl'] = $this->leafletService->generateAbsoluteString($feature['icon']['iconUrl']);
                        }
                      }
                      if (!empty($this->options['icon']['shadowUrl'])) {
                        $feature['icon']['shadowUrl'] = str_replace([
                          "\n",
                          "\r",
                        ], "", $this->viewsTokenReplace($this->options['icon']['shadowUrl'], $tokens));
                        if (!empty($feature['icon']['shadowUrl'])) {
                          $feature['icon']['shadowUrl'] = $this->leafletService->generateAbsoluteString($feature['icon']['shadowUrl']);
                        }
                      }

                      // Set Feature IconSize and ShadowSize to the IconUrl
                      // or ShadowUrl Image sizes (if empty or invalid).
                      $this->leafletService->setFeatureIconSizesIfEmptyOrInvalid($feature);

                      break;
                  }
                }

                // Associate dynamic path properties (token based) to each
                // feature, in case of not point.
                if ($feature['type'] !== 'point') {
                  $feature['path'] = str_replace([
                    "\n",
                    "\r",
                  ], "", $this->viewsTokenReplace($this->options['path'], $tokens));
                }

                // Associate dynamic className property (token based) to
                // icon.
                $feature['icon']['className'] = !empty($this->options['icon']['className']) ? str_replace([
                  "\n",
                  "\r",
                ], "", $this->viewsTokenReplace($this->options['icon']['className'], $tokens)) : '';

                // Add Feature additional Properties (if present).
                if (!empty($this->options['feature_properties']['values'])) {
                  $feature['properties'] = str_replace([
                    "\n",
                    "\r",
                  ], "", $this->viewsTokenReplace($this->options['feature_properties']['values'], $tokens));
                }

                // Add eventually the Marker Cluster Exclude Flag.
                if ($this->options['leaflet_markercluster'] && $this->options['leaflet_markercluster']['control'] && !empty($this->options['leaflet_markercluster']['excluded'])) {
                  $feature['markercluster_excluded'] = !empty(str_replace([
                    "\n",
                    "\r",
                  ], "", strip_tags($this->rendered_fields[$result->index][$this->options['leaflet_markercluster']['excluded']])));
                }

                // Eventually Add the belonging Group Label/Name to each
                // Feature, for possible based logics.
                if (count($view_results_groups) > 1) {
                  $feature['group_label'] = $group_label;
                }

                // Allow modules to adjust the single feature (marker).
                $this->moduleHandler->alter('leaflet_views_feature', $feature, $result, $this->view->rowPlugin);
              }
            }

            // Generate a single Features Group as incremental Features.
            $features_group = array_merge($features_group, $features);
          }
        }
        if ($features_group && count($features_group) > 1) {
          // Order the data features groups based on the 'weight' element.
          uasort($features_group, [
            'Drupal\Component\Utility\SortArray',
            'sortByWeightElement',
          ]);
        }

        // Generate Features Groups in case of Grouping.
        if (count($view_results_groups) > 1) {
          // Generate the Features Group.
          $group = [
            'group' => count($view_results_groups) > 1,
            'group_label' => $group_label,
            'disabled' => FALSE,
            'features' => $features_group,
            'weight' => 1,
          ];

          if (isset($this->options["grouping"][0]) && !empty($this->options["grouping"][0]["overlays_options"]["hidden_overlays_controls"])) {
            $group['group_label'] = !array_key_exists($group_label, $this->options["grouping"][0]["overlays_options"]["hidden_overlays_controls"]) ? $group_label : NULL;
          }

          if (isset($this->options["grouping"][0]) && !empty($this->options["grouping"][0]["overlays_options"]["disabled_overlays"])) {
            $group['disabled'] = array_key_exists($group_label, $this->options["grouping"][0]["overlays_options"]["disabled_overlays"]);
          }

          // Allow modules to adjust the single features group.
          $this->moduleHandler->alter('leaflet_views_features_group', $group, $this);

          // Add the Group to the Features Groups array/list.
          $features_groups[] = $group;
        }
      }

      // Order the data features groups based on the 'weight' element.
      if ($features_group && count($features_group) > 1) {
        // Order the data features groups based on the 'weight' element.
        uasort($features_group, [
          'Drupal\Component\Utility\SortArray',
          'sortByWeightElement',
        ]);
      }

      // Define the Js Settings.
      // Features is defined as Features Groups or single Features in case of a
      // single Features Group (no Grouping active)
      $js_settings = [
        'map' => $map,
        'features' => count($view_results_groups) > 1 ? $features_groups : ($features_group ?? []),
      ];

      // Allow other modules to add/alter the map js settings.
      $this->moduleHandler->alter('leaflet_map_view_style', $js_settings, $this);

      $map_height = !empty($this->options['height']) ? $this->options['height'] . $this->options['height_unit'] : '';
      if ((!empty($lat_value) && !empty($long_value)) || !($this->options['hide_empty_map'])) {
        $element = $this->leafletService->leafletRenderMap($js_settings['map'], $js_settings['features'], $map_height);
      }

      // Add the Core Drupal Ajax library for Ajax Popups.
      if (isset($map['settings']['ajaxPoup']) && $map['settings']['ajaxPoup']) {
        $build_for_bubbleable_metadata['#attached']['library'][] = 'core/drupal.ajax';
      }
      BubbleableMetadata::createFromRenderArray($element)
        ->merge(BubbleableMetadata::createFromRenderArray($build_for_bubbleable_metadata))
        ->applyTo($element);
    }

    return $element;
  }

  /**
   * Set default options.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['data_source_lat'] = ['default' => ''];
    $options['data_source_lon'] = ['default' => ''];
    $options['entity_source'] = ['default' => '__base_table'];
    $options['name_field'] = ['default' => ''];
    $options['description_field'] = ['default' => ''];
    $options['view_mode'] = ['default' => 'full'];

    $leaflet_map_default_settings = [];
    foreach (self::getDefaultSettings() as $k => $setting) {
      $leaflet_map_default_settings[$k] = ['default' => $setting];
    }
    return $options + $leaflet_map_default_settings;
  }

}
